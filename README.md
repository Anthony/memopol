[![Build Status](https://travis-ci.org/political-memory/political_memory.svg?branch=master)](https://travis-ci.org/political-memory/political_memory)
[![codecov.io](https://codecov.io/github/political-memory/political_memory/coverage.svg?branch=master)](https://codecov.io/github/political-memory/political_memory?branch=master)

[Manual](http://memopol.readthedocs.io/en/master/index.html)